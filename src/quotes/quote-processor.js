const fs = require("fs");

const regexQuotesSet = /(tags|\d)((\S|\s)*?)(Like)/;  //clean a little bit the very raw data
const regexTextQuotes = /―/                           //separate every quote from each other
const regexQuote = /“((\S|\s)*?)”/g                   //Select every single quote   **********The last quote/author must have a newline charset**********
const regexAuthor = /(―)\s((\S|\s)*?)()(?=(\s{2}))/g    //Select every single author  **********The last quote/author must have a newline charset**********
const regexEliminateSpace = /(\r\n|\n|\r)/gm          // Eliminate those horrendous spaces   
const quoteEndCode = "charserEndHere" // string markup to know where finish a quote

//reading file with unprocessed quotes copied directly from https://www.goodreads.com/quotes
const reader = fs.readFileSync("./quotes-raw.txt", "utf8");
// regex to apply split() on reader file  
// function to process the text.
const processor = file => {
    // split text into chunks with the quote and author.
    return file.split(regexQuotesSet)
    //filter the splited quotes to eliminete the rest of non-quote components in the array
    .filter(quote => {
        return regexTextQuotes.test(quote)
    })
    .join("")
   
}

const dataFunc = async () => {
    
    try {
        
        const data = await processor(reader).replace(/(\r\n|\n|\r)/gm," ")
        const authors = await data.match(regexAuthor)
        const quotes = await data.match(regexQuote)
        let quotesJson = []   
        for (let i = 0; i < quotes.length; i++) {
            quotesJson = [...quotesJson,  {quote : quotes[i] , author : authors[i]}]
        }
        return quotesJson
    } catch (error) {
        console.log(error)
    }
}

const RandomQuote = async () => {
    const processedData = await dataFunc()
    console.log(processedData[Math.floor(Math.random() * processedData.length)])
}

RandomQuote()

// const {author, quotes} = dataFunc()


// const quotesProcessed = quotes.map((quote, index) => {
//     ({quote , author : authors[index]})
//     console.log(1)
// })

// fs.writeFileSync("quotes-processed.txt", quotesProcessed)
// // console.log("DATA \n" + data)



